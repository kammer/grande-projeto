import java.util.Iterator;
import java.util.List;

public class FindGuitarTester {

    public static void main(String[] args) {
        Inventory inventory = new Inventory();
        initializeInventory(inventory);

        InstrumentSpec guitarRequest = new GuitarSpec(Builder.GIBSON,"Stratocaster", Type.ELECTRIC, 6, Wood.ALDER, Wood.ALDER);

        List guitarsFound = inventory.search((GuitarSpec) guitarRequest);

        if (!guitarsFound.isEmpty()) {

            System.out.println("Erin, as seguintes guitarras foram encontradas com esta especificação:");

            for (Iterator i = guitarsFound.iterator(); i.hasNext(); ) {

                Guitar guitar = (Guitar) i.next();
                InstrumentSpec spec = guitar.getSpec();
                GuitarSpec guitarSpec = (GuitarSpec) guitar.getSpec();

                System.out.println("  Você tem uma " + spec.getBuilder() + " " + spec.getModel() + " " + spec.getType() + " guitar:\n     Madeira " + spec.getBackWood() + " atrás e nos lados,\n     Madeira " + spec.getTopWood() + " na face.\n       com "+ guitarSpec.getNumStrings() +" cordas!\n  Você pode compra-lá pela bagatela de R$" + guitar.getPrice() + "!\n  ----");
            }
        } else {
            System.out.println("Foi mal Erin, não foi encontrado nada com estas especificações no estoque!");
        }

        InstrumentSpec mandolinRequest = new MandolinSpec(Builder.FENDER, "Stratocastor", Type.ELECTRIC, Style.A, Wood.ALDER, Wood.ALDER);

        List mandolinsFound = inventory.search((MandolinSpec) mandolinRequest);

        if (!mandolinsFound.isEmpty()) {

            System.out.println("Erin, os seguintes bandolins foram encontradas com esta especificação:");

            for (Iterator i = mandolinsFound.iterator(); i.hasNext(); ) {

                Mandolin mandolin = (Mandolin) i.next();
                InstrumentSpec spec = mandolin.getSpec();
                MandolinSpec mandolinSpec = (MandolinSpec) mandolin.getSpec();

                System.out.println("  Você tem um " + spec.getBuilder() + " " + spec.getModel() + " " + spec.getType() + " bandolin:\n     Madeira " + spec.getBackWood() + " atrás e nos lados,\n     Madeira " + spec.getTopWood() + " na face.\n       com o estilo: "+ mandolinSpec.getStyle() +"\n  Você pode compra-lá pela bagatela de R$" + mandolin.getPrice() + "!\n  ----");
            }
        } else {
            System.out.println("Foi mal Erin, não foi encontrado nada com estas especificações no estoque!");
        }
    }

    private static void initializeInventory(Inventory inventory) {
        inventory.addInstrument("11277", 3999.95, new GuitarSpec(Builder.COLLINGS, "CJ", Type.ACOUSTIC, 6, Wood.INDIAN_ROSEWOOD, Wood.SITKA));

        inventory.addInstrument("V95693", 1499.95, new MandolinSpec(Builder.FENDER, "Stratocastor", Type.ELECTRIC, Style.A, Wood.ALDER, Wood.ALDER));
    }
}
